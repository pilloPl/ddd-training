package io.pillopl.dddtraining.lending.infrastructure;


import io.pillopl.dddtraining.lending.domain.patron.Patron;
import io.pillopl.dddtraining.lending.domain.patron.PatronId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
public class PatronJdbcRepository {//implements PatronRepository {

    private final JdbcTemplate jdbcTemplate;

    //@Override
    public Optional<Patron> findById(PatronId patronId) {
        //TODO SQL SELECT
        PatronEntity entity = jdbcTemplate.queryForObject("", new BeanPropertyRowMapper<>(PatronEntity.class), patronId.getUuid());
        return Optional.empty();
    }

    //@Override
    @Transactional
    public void save(Patron patron) {
        insertPatron(patron);
        //tu bysmy dodali dodawanie Holds ... insertHolds() - batchowo
    }

    private void insertPatron(Patron patron) {
        //TODO SQL INSERT
        jdbcTemplate.update("", null, null);
    }


}


@NoArgsConstructor
@Data
class PatronEntity {
    UUID patron_id;
    //PatronType patronType;

}