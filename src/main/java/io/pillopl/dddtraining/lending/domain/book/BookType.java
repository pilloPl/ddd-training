package io.pillopl.dddtraining.lending.domain.book;

public enum BookType {
    Restricted, Circulating
}
