package io.pillopl.dddtraining.lending.domain.patron;

import io.pillopl.dddtraining.catalogue.BookId;
import io.pillopl.dddtraining.lending.domain.book.AvailableBook;

import java.util.UUID;

import static io.pillopl.dddtraining.lending.domain.book.BookType.Circulating;
import static io.pillopl.dddtraining.lending.domain.book.BookType.Restricted;

public class Fixture {

    public static Patron aRegularPatron() {
        //TODO IMPLEMENT
        return new Patron(new PatronId(UUID.randomUUID()));
    }

    public static Patron aResearcherPatron() {
        //TODO IMPLEMENT
        return new Patron(new PatronId(UUID.randomUUID()));
    }

    public static BookId anyBookId() {
        return new BookId(UUID.randomUUID());
    }

    public static AvailableBook restrictedBook() {
        return new AvailableBook(anyBookId(), Restricted);
    }

    public static AvailableBook circulatingBook() {
        return new AvailableBook(anyBookId(), Circulating);
    }
}
