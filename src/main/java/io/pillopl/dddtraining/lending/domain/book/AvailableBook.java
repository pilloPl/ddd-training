package io.pillopl.dddtraining.lending.domain.book;

import io.pillopl.dddtraining.catalogue.BookId;
import lombok.NonNull;
import lombok.Value;

@Value
public class AvailableBook {

    @NonNull BookId bookId;
    @NonNull BookType bookType;

    public boolean isRestricted() {
        return bookType.equals(BookType.Restricted);
    }
}

