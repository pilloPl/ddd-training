package io.pillopl.dddtraining.lending.infrastructure

import io.pillopl.dddtraining.lending.domain.patron.Patron
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType
import spock.lang.Specification

import javax.sql.DataSource

import static io.pillopl.dddtraining.lending.domain.patron.Fixture.aRegularPatron

class PatronJdbcRepositoryIT extends Specification {

    PatronJdbcRepository patronRepository

    def setup() {
        DataSource dataSource =
                new EmbeddedDatabaseBuilder()
                        .generateUniqueName(true)
                        .setType(EmbeddedDatabaseType.H2)
                        .addScript("create_patron_db.sql")
                        .build()
        patronRepository = new PatronJdbcRepository(new JdbcTemplate(dataSource))
    }

    def 'persistence in real database should work'() {
        given:
            Patron patron = aRegularPatron()
        when:
            patronRepository.save(patron)
        then:
            patronRepository.findById(patron.getPatronId()).isPresent()

    }

}
