package io.pillopl.dddtraining.lending.application;

enum Result {

    Allowance, Rejection
}
