package io.pillopl.dddtraining.lending.domain.patron

import io.pillopl.dddtraining.lending.domain.book.AvailableBook
import spock.lang.Specification

import static io.pillopl.dddtraining.lending.domain.patron.Fixture.aRegularPatron
import static io.pillopl.dddtraining.lending.domain.patron.Fixture.circulatingBook

class ExpiringHoldTest extends Specification {

    def 'can expire an existing hold'() {
        given:
            Patron patron = aRegularPatron()
        and:
            AvailableBook book = circulatingBook()
        and:
            patron.placeOnHold(book)
        when:
            Optional<BookHoldExpired> event = patron.expireHold(book.bookId)
        then:
            event.isPresent()
    }


    def 'cannot checkout when book is not on hold'() {
        given:
            Patron patron = aRegularPatron()
        and:
            AvailableBook book = circulatingBook()
        when:
            Optional<BookHoldExpired> event = patron.expireHold(book.bookId)
        then:
            event.isEmpty()
    }


}
