CREATE TABLE IF NOT EXISTS
  patron_entity
    (id INTEGER IDENTITY PRIMARY KEY,
    patron_type VARCHAR(100) NOT NULL,
    patron_id UUID UNIQUE);

CREATE TABLE IF NOT EXISTS
  hold_entity
  (id INTEGER IDENTITY PRIMARY KEY,
  book_id UUID NOT NULL,
  patron_id UUID NOT NULL,
  library_branch_id UUID NOT NULL,
  patron_entity INTEGER NOT NULL);

CREATE SEQUENCE patron_seq;
