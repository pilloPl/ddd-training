package io.pillopl.dddtraining.catalogue;

import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
public class BookId {

    @NonNull UUID uuid;
}
