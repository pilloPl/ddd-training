package io.pillopl.dddtraining.lending.domain.patron;

import io.pillopl.dddtraining.DomainEvent;
import io.pillopl.dddtraining.catalogue.BookId;
import io.pillopl.dddtraining.lending.domain.book.AvailableBook;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.util.Optional;

@AllArgsConstructor
public class Patron {

    @NonNull
    @Getter
    final PatronId patronId;

    //TODO IMPLEMENT
    public Optional<PlacedOnHold> placeOnHold(AvailableBook book) {
        return Optional.of(new PlacedOnHold());
    }

    //TODO IMPLEMENT
    public Optional<BookCheckedOut> checkout(BookId bookId) {
        return Optional.empty();
    }

    //TODO IMPLEMENT
    public Optional<BookHoldExpired> expireHold(BookId bookId) {
        return Optional.empty();
    }

}



class PlacedOnHold implements DomainEvent {

}

class BookCheckedOut implements DomainEvent {

}

class BookHoldExpired implements DomainEvent {

}
