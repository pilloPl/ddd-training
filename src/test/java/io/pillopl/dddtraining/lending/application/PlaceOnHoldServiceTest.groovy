package io.pillopl.dddtraining.lending.application

import io.pillopl.dddtraining.catalogue.BookId
import io.pillopl.dddtraining.lending.domain.patron.Fixture
import io.pillopl.dddtraining.lending.domain.patron.Patron
import spock.lang.Specification

import static io.pillopl.dddtraining.lending.domain.patron.Fixture.aRegularPatron

class PlaceOnHoldServiceTest extends Specification {

    BookId anyBook = Fixture.anyBookId()
    PlaceOnHoldService placeOnHoldService = new PlaceOnHoldService()

    def 'should allow when patron and available book exists'() {
        given:
            Patron patron = existingPatron()
        and:
            thereIsAvailableBook()
        expect:
            placeOnHoldService.placeOnHold(new PlaceOnHoldCommand(anyBook, patron.patronId)) == Result.Allowance
    }


    def 'should not allow when there is no patron'() {
        given:
            Patron patron = nonExistingPatron()
        and:
            thereIsAvailableBook()
        expect:
            placeOnHoldService.placeOnHold(new PlaceOnHoldCommand(anyBook, patron.patronId)) == Result.Rejection
    }

    def 'should not allow when there is no available book'() {
        given:
            Patron patron = existingPatron()
        and:
            thereIsNoAvailableBook()
        expect:
            placeOnHoldService.placeOnHold(new PlaceOnHoldCommand(anyBook, patron.patronId)) == Result.Rejection
    }

    void thereIsAvailableBook() {
        //TODO implement
    }

    void thereIsNoAvailableBook() {
        //TODO implement
    }

    Patron existingPatron() {
        //TODO implement
        Patron patron = aRegularPatron()
        return patron
    }

    Patron nonExistingPatron() {
        return aRegularPatron()
    }
}
