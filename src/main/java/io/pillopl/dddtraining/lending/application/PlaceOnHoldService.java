package io.pillopl.dddtraining.lending.application;

import io.pillopl.dddtraining.catalogue.BookId;
import io.pillopl.dddtraining.lending.domain.patron.PatronId;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;

@AllArgsConstructor
class PlaceOnHoldService {

    //TODO IMPLEMENT

    Result placeOnHold(PlaceOnHoldCommand command) {

        return Result.Allowance;

    }

}


@Value
class PlaceOnHoldCommand {

    @NonNull BookId bookId;
    @NonNull PatronId patronId;
}