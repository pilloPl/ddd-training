package io.pillopl.dddtraining.lending.domain.patron

import spock.lang.Specification

import static io.pillopl.dddtraining.lending.domain.patron.Fixture.aRegularPatron
import static io.pillopl.dddtraining.lending.domain.patron.Fixture.aResearcherPatron
import static io.pillopl.dddtraining.lending.domain.patron.Fixture.circulatingBook
import static io.pillopl.dddtraining.lending.domain.patron.Fixture.restrictedBook

class PlacingOnHoldTest extends Specification {


    def 'can place on hold circulating book if patron is regular'() {
        given:
            Patron patron = aRegularPatron()
        when:
            Optional<PlacedOnHold> placedOnHold = patron.placeOnHold(circulatingBook())
        then:
            placedOnHold.isPresent()
    }

    def 'cannot place on hold restricted book if patron is regular'() {
        given:
            Patron patron = aRegularPatron()
        when:
            Optional<PlacedOnHold> placedOnHold = patron.placeOnHold(restrictedBook())
        then:
            placedOnHold.isEmpty()
    }

    def 'can place on hold restricted book if patron is researcher'() {
        given:
            Patron patron = aResearcherPatron()
        when:
            Optional<PlacedOnHold> placedOnHold = patron.placeOnHold(restrictedBook())
        then:
            placedOnHold.isPresent()
    }

    def 'can place on hold up to 5 books'() {
        given:
            Patron patron = aResearcherPatron()
        and:
            5.times { patron.placeOnHold(circulatingBook())}
        when:
            patron.placeOnHold(circulatingBook())
        then:
            thrown(IllegalStateException)
    }

    def 'can place open-ended hold if patron is researcher'() {
        given:
            Patron patron = aResearcherPatron()
        when:
            Optional<PlacedOnHold> placedOnHold = patron.placeOnHold(restrictedBook())
        then:
            placedOnHold.isPresent()
    }

    def 'cannot place open-ended hold if patron is regular'() {
        //TODO implement
    }


    def 'can place close-ended hold if patron is researcher'() {
        //TODO implement
    }
}
